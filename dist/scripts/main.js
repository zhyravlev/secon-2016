'use strict';

$(function () {

  var html = $('html'),
      body = $('body'),
      popup_menu = $('.popup-menu'),
      numbers = $('#numbers'),
      navigation = $('.navigation'),
      speaker_dialog = $('#speakerDetails'),
      speakers_home = $('#speakersHome'),
      speakers_all = $('.masonry'),
      online_now = $('#onlineNow'),
      sign_in_link = $('#sign-in'),
      popup_sign_in = $('.popup-sign-in'),
      popup_sign_in_close_btn = popup_sign_in.find('.close'),
      schedule_header = $('#scheduleHeader');

  /* Detect iOS */

  /ipad|iphone|ipod/i.test(navigator.userAgent) ? html.addClass('is-ios') : false;

  /* Toggle menu */

  $('.toggle-menu').on('click', document, function () {

    $(this).toggleClass('in');
    popup_menu.toggleClass('in');
    body.toggleClass('popup-menu-is-show');

    if (Modernizr.touch) document.ontouchmove = document.ontouchmove === null ? function (event) {
      event.preventDefault();
    } : null;
  });

  /* Navigation */

  navigation.find('li').has('ul').children('a').on('click', document, function (e) {
    e.preventDefault();
    $(this).parent('li').not('.disabled').toggleClass('open').children('ul').slideToggle(250);
    $(this).parent('li').not('.disabled').siblings().removeClass('open').children('ul').slideUp(250);
  });

  /* For horizontal scroll numbers in popup menu */

  numbers.owlCarousel({
    margin: 2,
    loop: false,
    nav: false,
    responsive: { 0: { items: 4 }, 400: { items: 6 }, 600: { items: 8 } }
  });

  /* Speakers list in home page */

  speakers_home.owlCarousel({
    onInitialized: function onInitialized() {
      var next = $('<div/>').addClass('btn-next');
      speakers_home.append(next);
      next.on('click', function () {
        speakers_home.trigger('next.owl.carousel');
      });
    },
    touchDrag: false,
    loop: true,
    nav: false,
    responsive: { 0: { items: 1 }, 400: { items: 2 }, 800: { items: 3 }, 1000: { items: 4 }, 1200: { items: 5 } }
  });

  /* Masonry init */

  speakers_all.masonry({
    itemSelector: '.item',
    gutter: 0,
    percentPosition: true
  });

  /* Fix for touch devise */

  if (Modernizr.touch) {
    speakers_all.find('.item').hammer().bind("tap", function () {
      return true;
    });
  }

  /* Toggle blur effect */

  speaker_dialog.on('show.bs.modal', function () {
    body.toggleClass('popup-menu-is-show');
  }).on('hide.bs.modal', function () {
    speaker_dialog.removeData('bs.modal');
    body.toggleClass('popup-menu-is-show');
  });

  /* Show a pop-up window with information about the speaker */

  var math;

  if ((math = /^(#\/[speaker]+\/)(\d+)(\/)$/i.exec(window.location.hash)) !== null && math[2]) {

    console.info('Speaker id:', math[2]);

    var url_for_load = 'dialog.content.html';

    $.get(url_for_load, function (data) {
      speaker_dialog.find('.modal-content').html(data);
      speaker_dialog.modal('show');
    });
  }

  /* Schedule */

  var scheduleHeader = $('#scheduleHeader');
  var scheduleOptions = {
    loop: true,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1200: {
        items: 4
      }
    }
  };

  var allOwl = $('.owl-carousel.data');
  allOwl.owlCarousel(scheduleOptions);

  scheduleHeader.owlCarousel({
    onInitialize: function onInitialize() {
      if (schedule_header.find('.item').size() > 4) {
        body.addClass('show-schedule-btn');
      }
    },
    onInitialized: function onInitialized() {

      var prev = $('<div/>').addClass('btn-prev').html('<i class="icon-prev"></i>'),
          next = $('<div/>').addClass('btn-next').html('<i class="icon-next"></i>');

      scheduleHeader.prepend(prev);
      scheduleHeader.append(next);

      prev.on('click', function () {
        scheduleHeader.trigger('prev.owl.carousel');
        allOwl.trigger('prev.owl.carousel');
      });
      next.on('click', function () {
        scheduleHeader.trigger('next.owl.carousel');
        allOwl.trigger('next.owl.carousel');
      });
    },
    loop: true,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1200: {
        items: 4
      }
    }
  });

  /* Toggle description in schedule */

  var item_more = $('.item.more');

  item_more.on('click', function () {

    var id = $(this).data('show-id'),
        row = $('#desc_' + id),
        schedule_description = $('.schedule.description');

    item_more.hasClass('active') ? item_more.removeClass('active') : false;

    if (schedule_description.is(':visible')) {
      schedule_description.slideUp(200);
    }

    if (!row.is(':visible')) {
      $(this).addClass('active');
      row.slideDown(200);
    }
  });

  /* Fancy box for video */

  $("a.video").click(function (event) {

    $.fancybox({
      'padding': 0,
      'autoScale': false,
      'transitionIn': 'none',
      'transitionOut': 'none',
      'title': this.title,
      'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
      'type': 'swf',
      'swf': {
        'wmode': 'transparent',
        'allowfullscreen': 'true'
      },
      beforeShow: function beforeShow() {
        body.toggleClass('popup-menu-is-show');
      },
      beforeClose: function beforeClose() {
        body.toggleClass('popup-menu-is-show');
      }
    });

    event.preventDefault();
  });

  /* Now online */

  var toggle_nex_btn = function toggle_nex_btn() {
    if (online_now.find('.owl-stage').width() > online_now.find('.owl-stage-outer').width()) {
      online_now.find('.btn-next').show();
    } else {
      online_now.find('.btn-next').hide();
    }
    return true;
  };

  online_now.owlCarousel({
    onInitialized: function onInitialized() {

      var next = $('<div/>').addClass('btn-next');
      online_now.append(next);
      next.on('click', function () {
        online_now.trigger('next.owl.carousel');
      });

      toggle_nex_btn();
    },
    onResized: toggle_nex_btn,
    touchDrag: true,
    loop: false,
    nav: false,
    responsive: { 0: { items: 1 }, 400: { items: 2 }, 800: { items: 3 }, 1000: { items: 4 }, 1200: { items: 5 } }
  });

  /* Drop down field */

  $.fn.dropdown.settings.message.noResults = 'Ничего не найдено...';
  $('#search-select').dropdown();

  /* Fancy box for photos */

  $('a.fancybox-photo').fancybox({
    'padding': 0
  });

  /* Show pop up sign in form */

  var toggle_sign_in = function toggle_sign_in() {
    popup_sign_in.toggleClass('in');
    body.toggleClass('popup-menu-is-show');

    if (Modernizr.touch) document.ontouchmove = document.ontouchmove === null ? function (event) {
      event.preventDefault();
    } : null;
  };

  sign_in_link.on('click', function (event) {
    event.preventDefault();
    toggle_sign_in();
  });

  popup_sign_in_close_btn.on('click', function () {
    toggle_sign_in();
  });

  /* Users table */

  var usersOptions = {
    loop: false,
    nav: false,
    responsive: {
      0: {
        items: 1
      },
      500: {
        items: 2
      },
      600: {
        items: 3
      },
      1200: {
        items: 5,
        autoWidth: true
      }
    }
  };

  var usersOwl = $('.owl-carousel.user');
  usersOwl.owlCarousel(usersOptions);

  /* rating */

  $('div[data-rating="true"]').each(function (a, b) {

    var id = $(b).data('rating-id');
    var el = document.querySelector('div[data-rating-id="' + id + '"]');
    var current = Number($(b).data('rating-current'));

    return rating(el, current, 5, function (rating) {
      $.ajax({
        'url': '?rating=' + rating + '&item_id=' + id,
        'method': 'GET',
        'cache': false,
        'dataType': 'json',
        'success': function success(response) {
          console.log(response);
        }
      });
    });
  });

  /* Fixed footer */

  var container_full = $('.container-full');

  var setFooter = function setFooter() {
    container_full.css({
      'min-height': $(window).height() - 80 + 'px'
    });
  };

  $(document).ready(setFooter);
  $(window).on('resize', setFooter);
});
//# sourceMappingURL=../maps/main.js.map
